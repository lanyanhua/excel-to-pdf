package com.lanyanhua.excel.exceltopdf;

import org.apache.poi.xssf.usermodel.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

@RestController
@SpringBootApplication
public class ExcelToPdfApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExcelToPdfApplication.class, args);
    }


    @PostMapping("/excelToPdf")
    public void excelToPdf(MultipartFile excel, HttpServletResponse response) throws Exception{
        ByteArrayInputStream is = new ByteArrayInputStream(excel.getBytes());
        XSSFWorkbook wb = new XSSFWorkbook(is);
        //修改excel
        //设置输出格式
        response.reset();
        response.setContentType("application/octet-stream;charset=UTF-8");
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("statements.pdf", "utf-8"));
        OutputStream outputStream = response.getOutputStream();
        //转pdf输出
        ExcelUtil.toPdf(wb,outputStream);
        outputStream.flush();
        outputStream.close();
    }

    @PostMapping("/excelToPdf1")
    public void excelToPdf1(HttpServletResponse response) throws Exception{
        XSSFWorkbook wb = new XSSFWorkbook();
        //修改excel
        XSSFSheet sheet = wb.createSheet();
        XSSFRow row = sheet.createRow(0);
        row.setHeightInPoints(50);
        //设置大小，
        sheet.setColumnWidth(0, 19000);
        XSSFCell cell = row.createCell(0);
        cell.setCellValue("我是PDF");
        XSSFCellStyle cellStyle = wb.createCellStyle();
        XSSFFont font = wb.createFont();
        font.setFontHeightInPoints((short) 20);
        font.setBold(true);
        cellStyle.setFont(font);
        cell.setCellStyle(cellStyle);
        //设置输出格式
        response.reset();
        response.setContentType("application/octet-stream;charset=UTF-8");
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("statements.pdf", "utf-8"));
        OutputStream outputStream = response.getOutputStream();
        //转pdf输出
        //本地改流就好了
        //OutputStream outputStream = new FileOutputStream("/Users/lanyanhua/Desktop/本地文件.pdf");
        ExcelUtil.toPdf(wb,outputStream);
        outputStream.flush();
        outputStream.close();
    }



}
